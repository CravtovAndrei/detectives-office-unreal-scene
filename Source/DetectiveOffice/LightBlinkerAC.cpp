// Fill out your copyright notice in the Description page of Project Settings.


#include "LightBlinkerAC.h"

// Sets default values for this component's properties
ULightBlinkerAC::ULightBlinkerAC()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void ULightBlinkerAC::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void ULightBlinkerAC::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool ULightBlinkerAC::RandomizedBool()
{
	int RandomNumber = FMath::RandRange(MinBlinkRange, MaxBlinkRange);
	int MiddleNumber = MaxBlinkRange / 2;
	if(RandomNumber > (MiddleNumber / BlinkSpeed)) return true;
	return false;
}

