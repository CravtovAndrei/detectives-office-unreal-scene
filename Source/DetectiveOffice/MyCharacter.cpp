// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCharacter.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"

// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HoldingComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Holding Component"));
	HoldingComponent->SetRelativeLocation(FVector(50.0f,0,0));
	HoldingComponent->SetupAttachment(HoldLocation);

	CurrentItem = nullptr;
	bCanMove = true;
	bInspecting = false;
}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();

	PitchMax = GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMax;
	PitchMin = GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMin;
}

// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("TakeObjectAction", IE_Pressed, this, &AMyCharacter::OnAction);
	PlayerInputComponent->BindAction("Inspect", IE_Pressed, this, &AMyCharacter::OnInspect);
	PlayerInputComponent->BindAction("Inspect", IE_Released, this, &AMyCharacter::OnInspectReleased);
	PlayerInputComponent->BindAction("Inspect", IE_Released, this, &AMyCharacter::ResetHeldActorPosition);
}

// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(FirstPersonCameraComponent)
	{
		Start = FirstPersonCameraComponent->GetComponentLocation();
		ForwardVector = FirstPersonCameraComponent->GetForwardVector();
		End = (ForwardVector * 200.f) + Start;
	}

	if(!bHoldingItem)
	{
		if(GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams))
		{
			if(Hit.GetActor()->GetClass()->IsChildOf(APickupAndRotateActor::StaticClass()))
			{
				CurrentItem = Cast<APickupAndRotateActor>(Hit.GetActor());
			}
		}
		else
		{
			
			CurrentItem = nullptr;
		}
	}

	if(bInspecting)
	{
		if(bHoldingItem && FirstPersonCameraComponent)
		{
			FirstPersonCameraComponent->SetFieldOfView(FMath::Lerp(FirstPersonCameraComponent->FieldOfView, 90.f, 0.1f));
			HoldingComponent->SetRelativeLocation(FVector(0.0f,50.f, 50.f));
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMax = 179.900000f;
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMin = -179.900000f;
			CurrentItem->RotateActor();
		}
		
		else if(FirstPersonCameraComponent)
		{
			FirstPersonCameraComponent->SetFieldOfView(FMath::Lerp(FirstPersonCameraComponent->FieldOfView, 45.f, 0.1f));
		}
	}
	else if(FirstPersonCameraComponent)
	{
		FirstPersonCameraComponent->SetFieldOfView(FMath::Lerp(FirstPersonCameraComponent->FieldOfView, 90.f, 0.1f));

		if(bHoldingItem) HoldingComponent->SetRelativeLocation(FVector(50.f, 0.0f, 0.0f));
	}

}






void AMyCharacter::OnAction()
{
	//todo fix bug with bInspecting is never true
	if(CurrentItem && !bInspecting)
	{
		ToggleItemPickup();
	}
}

void AMyCharacter::ToggleItemPickup()
{
	if(CurrentItem){
		bHoldingItem = !bHoldingItem;
		CurrentItem->PickUp();

		if(!bHoldingItem)
		{
			CurrentItem = nullptr;
		}
	}

}

void AMyCharacter::ToggleMovement()
{
	bCanMove = !bCanMove;
	bInspecting = !bInspecting;
	FirstPersonCameraComponent->bUsePawnControlRotation = !FirstPersonCameraComponent->bUsePawnControlRotation;
	bUseControllerRotationYaw = !bUseControllerRotationYaw;
}

void AMyCharacter::OnInspect()
{
	if(bHoldingItem)
	{
		LastRotation = GetControlRotation();
		ToggleMovement();
	}
	else
	{
		bInspecting = true;
	}
}

void AMyCharacter::OnInspectReleased()
{
	if(bInspecting && bHoldingItem)
	{
		GetController()->SetControlRotation(LastRotation);
		GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMax = PitchMax;
		GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMin = PitchMin;
		ToggleMovement();
	}
	else
	{
		bInspecting = false;
	}
}

void AMyCharacter::ResetHeldActorPosition()
{
	if(CurrentItem)
	{
		CurrentItem->ResetToStartLocationRotation();
	}
}
