// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Camera/CameraComponent.h"
#include "PickupAndRotateActor.generated.h"

UCLASS()
class DETECTIVEOFFICE_API APickupAndRotateActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickupAndRotateActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void SaveStartLocationRotation();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void ResetToStartLocationRotation();

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* MyMesh;
	
	UPROPERTY(EditAnywhere)
	USceneComponent* HoldingComp;

	UFUNCTION(BlueprintCallable)
	void PickUp();

	UFUNCTION(BlueprintCallable)
	void RotateActor();

	bool bHolding;
	bool bGravity;

	FRotator StartRotation;
	FVector StartPosition;

	FRotator ControlRotation;
	ACharacter* MyCharacter;
	UCameraComponent* PlayerCamera;
	FVector ForwardVector;
};
