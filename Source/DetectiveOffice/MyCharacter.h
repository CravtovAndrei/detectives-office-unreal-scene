// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PickupAndRotateActor.h"
#include "MyCharacter.generated.h"

UCLASS()
class DETECTIVEOFFICE_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	void ToggleItemPickup();
	void OnAction();
	void ToggleMovement();
	void OnInspect();
	void OnInspectReleased();
	void ResetHeldActorPosition();
	

public:	
	// Called every frame
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* HoldLocation;

	UPROPERTY(EditAnywhere)
	class USceneComponent* HoldingComponent;
	
	UPROPERTY(EditAnywhere)
	class APickupAndRotateActor* CurrentItem;
	
	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(BlueprintReadWrite)
	bool bCanMove;
	
	UPROPERTY(BlueprintReadWrite)
	bool bCanRotate;
	
	bool bHoldingItem;
	bool bInspecting;

	

	FVector HoldingComp;
	FRotator LastRotation;

	FVector Start;
	FVector ForwardVector;
	FVector End;

	FHitResult Hit;

	FComponentQueryParams DefaultComponentQueryParams;
	FCollisionResponseParams DefaultResponseParams;

	float PitchMax;
	float PitchMin;
};
